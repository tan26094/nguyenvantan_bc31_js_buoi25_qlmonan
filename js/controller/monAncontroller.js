export let monAnController = {
  layThongTinTuForm: () => {
    let tenMon = document.getElementById("tenMon").value;
    let giaMon = document.getElementById("giaMon").value;
    let moTa = document.getElementById("moTa").value;
    // console.log({ tenMon, giaMon, moTa });
    let monAn = {
      name: tenMon,
      price: giaMon,
      description: moTa,
    };
    return monAn;
  },
  showThongTinLenForm: (monAn) => {
    document.getElementById("tenMon").value = monAn.name;
    document.getElementById("giaMon").value = monAn.price;
    document.getElementById("moTa").value = monAn.description;
  },
};
