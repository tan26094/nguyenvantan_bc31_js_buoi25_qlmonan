// https://62c2fe8eff594c65676b786c.mockapi.io/
// console.log(axios());
let foodList = [];
let idFoodEdited = null;
let renderTable = (list) => {
  let contentHTML = "";
  for (let index = 0; index < list.length; index++) {
    let monAn = list[index];
    let contentTr = `<tr> 
    <td> ${monAn.id} </td>
    <td> ${monAn.name} </td>
    <td> ${monAn.price} </td>
    <td> ${monAn.description} </td>
    <td> 
    <button class="btn btn-primary" onclick="suaMonAn(${monAn.id})">Sửa</button>
    <button class="btn btn-warning" onclick="xoaMonAn(${monAn.id})">Xóa</button>
    </td>
    </tr>`;
    contentHTML = contentHTML + contentTr;
  }
  // console.log("contentHTML", contentHTML);
  document.getElementById("tbody_food").innerHTML = contentHTML;
  // render danh sách món ăn ra ngoài màn hình
};
//------------------ Xóa món ăn
let xoaMonAn = (maMonAn) => {
  monAnService
    .xoaMonAn(maMonAn)
    .then((res) => {
      spinnerService.tatLoading();
      // console.log(res);
      // Sau khi xóa món ăn thàng công
      renderDanhSachService();
    })
    .catch((err) => {
      spinnerService.tatLoading();
      // console.log(err);
    });
};
window.xoaMonAn = xoaMonAn;
//------------------ sửa món ăn

let suaMonAn = (idMonAn) => {
  console.log(idMonAn);
  // Lấy id của món ăn được chọn gán vào idfoodEdited
  idFoodEdited = idMonAn;
  monAnService
    .layThongTinCHiTietMonAn(idMonAn)
    .then((res) => {
      spinnerService.tatLoading();
      // console.log(res.data);
      console.log(res.data);
      // console.log(foodList);
      monAnController.showThongTinLenForm(res.data);
    })
    .catch((err) => {
      spinnerService.tatLoading();
      console.log(err);
    });
};

window.suaMonAn = suaMonAn;
import { monAnController } from "./controller/monAncontroller.js";
//------------------------------------------------------ Lấy danh sách món ăn------------------------------------------------------

//------------------------------------------------------ Lấy danh sách món ăn và render danh sách ra màng hình------------------------------------------------------
import { monAnService } from "./service/monAnService.js";
import { spinnerService } from "./service/spinnerService.js";
let renderDanhSachService = () => {
  spinnerService.batLoading();
  monAnService
    .layDanhSachMonAn()
    .then((res) => {
      spinnerService.tatLoading();
      // console.log(res.data);
      foodList = res.data;
      // console.log(foodList);
      renderTable(foodList);
    })
    .catch((err) => {
      // console.log(err);
    });
};
// Chạy lần đầu khi load lại trang
renderDanhSachService();
// Thêm mới món ăn
let themMonAn = () => {
  let monAn = monAnController.layThongTinTuForm();
  spinnerService.batLoading();
  monAnService
    .themMonAn(monAn)
    .then((res) => {
      console.log(monAn);
      spinnerService.tatLoading();
      // alert("thành công");
      renderDanhSachService();
    })
    .catch((res) => {
      // alert("thất bại");
    });
};
window.themMonAn = themMonAn;
//------------------------------------Cập nhật món ăn------------------------------------------------------
let capNhatMonAn = () => {
  spinnerService.batLoading();
  let monAn = monAnController.layThongTinTuForm();
  let newMonAn = { ...monAn, id: idFoodEdited };
  console.log(monAn);
  monAnService
    .capNhatMonAn(newMonAn)
    .then((res) => {
      spinnerService.tatLoading();
      console.log(res);
      monAnController.showThongTinLenForm({
        name: "",
        price: "",
        description: "",
      });
      renderDanhSachService();
      // clear thông tin sau khi update thành công
    })
    .catch((err) => {
      spinnerService.tatLoading();
      console.log(err);
    });
};
window.capNhatMonAn = capNhatMonAn;

// ------------------------------------------------------setTimout bất đồng bộ------------------------------------------------------
// setTimeout(() => {
//   console.log("0");
// }, 1000);
// setTimeout(() => {
//   console.log("3");
// }, 100);
// console.log("1");
