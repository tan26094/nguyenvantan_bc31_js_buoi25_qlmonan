const BaseURL = "https://62c2fe8eff594c65676b786c.mockapi.io/mon-an/";
export let monAnService = {
  layDanhSachMonAn: () => {
    return axios({
      url: BaseURL,
      method: "GET",
    });
  },
  xoaMonAn: (id) => {
    return axios({
      url: `${BaseURL}${id}`,
      method: "DELETE",
    });
  },
  themMonAn: (monAn) => {
    return axios({
      url: BaseURL,
      method: "POST",
      data: monAn,
    });
  },
  layThongTinCHiTietMonAn: (id) => {
    return axios({
      url: `${BaseURL}${id}`,
      method: "GET",
    });
  },
  capNhatMonAn: (monAn) => {
    return axios({
      url: `${BaseURL}${monAn.id}`,
      method: "PUT",
      data: monAn,
    });
  },
};
