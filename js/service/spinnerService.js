export const spinnerService = {
  batLoading: () => {
    // Hiện loading
    document.getElementById("loading").style.display = "flex";
    // Ẩn table
    document.getElementById("content").style.display = "none";
  },
  tatLoading: () => {
    // Ẩn loading
    document.getElementById("loading").style.display = "none";
    // Hiện table
    document.getElementById("content").style.display = "block";
  },
};
